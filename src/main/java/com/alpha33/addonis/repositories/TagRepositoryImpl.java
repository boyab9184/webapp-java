package com.alpha33.addonis.repositories;

import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;

    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :searchedName", Tag.class);
            query.setParameter("searchedName", name);
            List<Tag> result = query.list();

            return result;
        }
    }

    @Override
    public Tag getByNameNew(String tagName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :searchedName", Tag.class);
            query.setParameter("searchedName", tagName);

            List<Tag> resultNew = query.list();
            if (resultNew.size() == 0) {
                return null;
            }
            return resultNew.get(0);
        }
    }

    @Override
    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }
}
