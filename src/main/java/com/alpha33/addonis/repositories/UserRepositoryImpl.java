package com.alpha33.addonis.repositories;

import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
            System.out.printf("User with id %d was deleted successfully%n", id);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<User> search(String searchParam) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " from User where  username like :username or email like :email or phone like :phone"
                    , User.class);

            query.setParameter("username", "%" + searchParam + "%");
            query.setParameter("email", "%" + searchParam + "%");
            query.setParameter("phone", "%" + searchParam + "%");


            return query.list();
        }
    }

    @Override
    public void updateUserStatus(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            if (user.getStatus() > 0) {
                user.setStatus(0);
            } else if (user.getStatus() == 0) {
                user.setStatus(1);
            }

            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public int getUserStatus(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user.getStatus();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where email like :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where username= :usernameToCompare"
                    , User.class);
            query.setParameter("usernameToCompare", username);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByPhone(String phone) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where phone= :phone"
                    , User.class);
            query.setParameter("phone", phone);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "phone number", phone);
            }
            return result.get(0);
        }
    }


}
