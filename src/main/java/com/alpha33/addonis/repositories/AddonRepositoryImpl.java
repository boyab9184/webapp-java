package com.alpha33.addonis.repositories;

import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class AddonRepositoryImpl implements AddonRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Addon> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon", Addon.class);
            return query.list();
        }
    }

    @Override
    public Addon getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Addon addon = session.get(Addon.class, id);
            if (addon == null) {
                throw new EntityNotFoundException("Addon", id);
            }
            return addon;
        }
    }

    @Override
    public Addon getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where name = :searchedName", Addon.class);
            query.setParameter("searchedName", name);

            List<Addon> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Addon", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public Addon getByFileName(String fileName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where fileName = :searchedName", Addon.class);
            query.setParameter("searchedName", fileName);

            List<Addon> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Addon", "fileName", fileName);
            }
            return result.get(0);
        }
    }

    @Override
    public List<Addon> getAllByUser(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(
                    "from Addon  where user.username = :searchedUsername order by uploadDate desc ",
                    Addon.class);
            query.setParameter("searchedUsername", username);

            return query.list();
        }
    }

    @Override
    public void create(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.save(addon);
        }
    }

    @Override
    public void update(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(addon);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Addon addonToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(addonToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Addon> filter(Optional<String> ide, Optional<String> name, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from Addon ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            ide.ifPresent(value -> {
                filters.add("ide.name = :ide");
                params.put("ide", value);
            });

            name.ifPresent(value -> {
                filters.add("name like :name");
                params.put("name", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append("where ")
                        .append(String.join(" and ", filters));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortingString(value));
            });

            System.out.println(queryString);
            Query<Addon> query = session.createQuery(queryString.toString(), Addon.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateSortingString(String value) {
        StringBuilder result = new StringBuilder(" order by ");
        var params = value.toLowerCase().split("_");

        switch (params[0]) {
            case "name":
                result.append("name ");
                break;
            case "downloads":
                result.append("downloads ");
                break;
            case "upload date":
                result.append("date(uploadDate)");
                break;
            default:
                return "";
        }

        if (params.length > 1 && params[1].equals("desc")) {
            result.append("desc");
        }
        return result.toString();
    }

    @Override
    public List<Addon> getMostPopular() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.status = 'Approved' order by a.downloads DESC ", Addon.class);

            return query.stream().limit(5).collect(Collectors.toList());
        }
    }

    @Override
    public List<Addon> getNew() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.status = 'Approved' order by a.uploadDate DESC ", Addon.class);

            return query.stream().limit(5).collect(Collectors.toList());
        }
    }

    @Override
    public List<Addon> getFeatured() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.status = 'Approved' and featured = :highlighted", Addon.class);
            query.setParameter("highlighted", 1);

            return query.stream().limit(5).collect(Collectors.toList());
        }
    }

    @Override
    public List<Addon> getAllApproved() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.status = 'Approved'", Addon.class);
            return query.list();
        }
    }
}
