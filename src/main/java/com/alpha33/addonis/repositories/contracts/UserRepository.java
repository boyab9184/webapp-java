package com.alpha33.addonis.repositories.contracts;

import com.alpha33.addonis.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

    void delete(int id);

    void create(User user);

    void update(User user);

    List<User> search(String search);

    void updateUserStatus(int id);

    int getUserStatus(int id);

    User getByEmail(String email);

    User getByUsername(String username);

    User getByPhone(String phone);
}
