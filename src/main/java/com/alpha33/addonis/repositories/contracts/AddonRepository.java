package com.alpha33.addonis.repositories.contracts;

import com.alpha33.addonis.models.Addon;

import java.util.List;
import java.util.Optional;

public interface AddonRepository {

    List<Addon> getAll();

    Addon getById(int id);

    Addon getByName(String name);

    Addon getByFileName(String fileName);

    List<Addon> getAllByUser(String username);

    void create(Addon addon);

    void update(Addon addon);

    void delete(int id);

    List<Addon> filter(Optional<String> ide, Optional<String> name, Optional<String> sort);

    List<Addon> getMostPopular();

    List<Addon> getNew();

    List<Addon> getFeatured();

    List<Addon> getAllApproved();
}
