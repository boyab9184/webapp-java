package com.alpha33.addonis.repositories.contracts;

import com.alpha33.addonis.models.AddonRating;

import java.util.List;

public interface AddonRatingRepository {

    void create(AddonRating addonRating);

    List<AddonRating> getAll();

    double calculateRating(int id);

    long calculateRaters(int id);

    long[] ratingsByUsers(int id);
}
