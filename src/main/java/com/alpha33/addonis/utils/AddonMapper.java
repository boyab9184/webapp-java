package com.alpha33.addonis.utils;

import com.alpha33.addonis.models.*;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import com.alpha33.addonis.repositories.contracts.IdeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class AddonMapper {

    private final IdeRepository ideRepository;
    private final AddonRepository addonRepository;

    @Autowired
    public AddonMapper(IdeRepository ideRepository, AddonRepository addonRepository) {
        this.ideRepository = ideRepository;
        this.addonRepository = addonRepository;
    }

    public Addon convertAddonDtoToAddon(AddonBodyDTO addonBodyDTO, User user, byte[] image, byte[] content) {
        Addon newAddon = new Addon();
        mapValues(addonBodyDTO, image, content, newAddon, user);
        return newAddon;
    }

    public Addon convertAddonDtoToAddon(AddonBodyDTO addonBodyDTO, int id, byte[] image, byte[] content) {
        Addon newAddon = addonRepository.getById(id);
        User user = newAddon.getUser();
        mapValues(addonBodyDTO, image, content, newAddon, user);
        return newAddon;
    }

    private void mapValues(AddonBodyDTO addonBodyDTO, byte[] image, byte[] content, Addon newAddon, User user) {
        Ide ide = ideRepository.getById(addonBodyDTO.getIdeId());

        newAddon.setName(addonBodyDTO.addonName);
        newAddon.setIde(ide);
        newAddon.setOriginLocation(addonBodyDTO.getOriginLocation());
        newAddon.setDescription(addonBodyDTO.getDescription());
        newAddon.setUser(user);
        newAddon.setContent(content);
        newAddon.setImage(image);
        newAddon.setFileName(addonBodyDTO.getFileName());
    }

    public AddonResponseView convertToAddonResponseView(Addon addon) {
        DecimalFormat df = new DecimalFormat("#.##");
        double rating = addon.getRating();

        AddonResponseView addonResponseView = new AddonResponseView();
        addonResponseView.setAddonId(addon.getId().toString());
        addonResponseView.setAddonName(addon.getName());
        addonResponseView.setOriginLocation(addon.getOriginLocation());
        addonResponseView.setFileName(addon.getFileName());
        addonResponseView.setDescription(addon.getDescription());
        addonResponseView.setDownloads(addon.getDownloads().toString());
        addonResponseView.setIdeName(addon.getIde().getName());
        addonResponseView.setRating(df.format(rating));
        addonResponseView.setStatus(addon.getStatus().toString());
        addonResponseView.setUsername(addon.getUser().getUsername());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String dateOnly = dateFormat.format(addon.getUploadDate());
        addonResponseView.setUploadDate(dateOnly);
        return addonResponseView;
    }

    public List<AddonResponseView> convertAddonsListToAddonsResponseViewList(List<Addon> addons) {
        List<AddonResponseView> listWithAddonsResponseView = new ArrayList<>();
        for (Addon a : addons) {
            listWithAddonsResponseView.add(convertToAddonResponseView(a));
        }
        return listWithAddonsResponseView;
    }

    public Addon mapAddonNewData(Addon existingAddon, Addon addonNewData, MultipartFile contentFile) throws IOException {
        if (contentFile != null && contentFile.getOriginalFilename() != null && !contentFile.getOriginalFilename().equals("")) {
            existingAddon.setContent(contentFile.getBytes());
            existingAddon.setFileName(contentFile.getOriginalFilename());
        }
        existingAddon.setName(addonNewData.getName());
        existingAddon.setDescription(addonNewData.getDescription());
        existingAddon.setOriginLocation(addonNewData.getOriginLocation());
        return existingAddon;
    }
}
