package com.alpha33.addonis.utils;

import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

public class Utilities {

    public static byte[] getFileBytes(MultipartFile inputFile) {
        byte[] content = null;
        try {
            content = inputFile.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static String getAddonImage(Addon a) {
        String image;
        if (a.getImage() == null) {
            image = null;
        } else {
            image = "data:image/png;base64," + Base64.getEncoder().encodeToString(a.getImage());
        }
        return image;
    }

    public static String getImage(User user) {
        String image;
        if (user.getImage() == null) {
            image = null;
        } else {
            image = "data:image/png;base64," + Base64.getEncoder().encodeToString(user.getImage());
        }
        return image;
    }
}
