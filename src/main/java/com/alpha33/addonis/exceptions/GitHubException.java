package com.alpha33.addonis.exceptions;

public class GitHubException extends RuntimeException {

    public GitHubException(String message) {
        super(message);
    }
}
