package com.alpha33.addonis.controllers.mvc;

import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final AddonService addonService;

    public HomeMvcController(UserService userService, AuthenticationHelper authenticationHelper, AddonService addonService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.addonService = addonService;
    }

    @GetMapping
    public String showHomePage(HttpSession session, Model model) {
        User user;
        if (authenticationHelper.isUserLogged(session)) {
            user = authenticationHelper.tryGetUser(session);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            model.addAttribute("loggedUser", user);
        }

        List<Addon> featured = addonService.getFeatured();
        List<Addon> popular = addonService.getMostPopular();
        List<Addon> recent = addonService.getNew();
        addonService.setAddonCardData(featured);
        addonService.setAddonCardData(popular);
        addonService.setAddonCardData(recent);

        model.addAttribute("featured", featured);
        model.addAttribute("popular", popular);
        model.addAttribute("recent", recent);
        return "index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUserUsername") != null;
    }
}
