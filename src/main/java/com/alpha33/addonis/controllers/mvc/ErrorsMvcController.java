package com.alpha33.addonis.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/errors")
public class ErrorsMvcController {

    @GetMapping("/409-conflict")
    public String showConflict409() {
        return "/pages/409-conflict-duplicate-entry";
    }

    @GetMapping("/401-unauthorised")
    public String showUnauthorised401() {
        return "/pages/401-unauthorized-access";
    }

    @GetMapping("/400-bad_request")
    public String showBadRequest400() {
        return "/pages/400-bad-request";
    }
}
