package com.alpha33.addonis.controllers.mvc;


import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.AuthenticationFailureException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.Ide;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.services.contracts.IdeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

import static com.alpha33.addonis.utils.Utilities.getAddonImage;
import static com.alpha33.addonis.utils.Utilities.getImage;

@Controller
@RequestMapping("/addons")
public class AddonSearchFilterMvcController {

    private final AddonService addonService;
    private final AddonRatingService addonRatingService;
    private final AuthenticationHelper authenticationHelper;
    private final IdeService ideService;

    @Autowired
    public AddonSearchFilterMvcController(AddonService addonService,
                                          AddonRatingService addonRatingService,
                                          AuthenticationHelper authenticationHelper,
                                          IdeService ideService) {
        this.addonService = addonService;
        this.addonRatingService = addonRatingService;
        this.authenticationHelper = authenticationHelper;
        this.ideService = ideService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUserUsername") != null;
    }


    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String getAllAddons(Model model,
                               @RequestParam(value = "ide", required = false) String ide,
                               @RequestParam(value = "search", required = false) String name,
                               @RequestParam(value = "sort", required = false) String sort,
                               HttpSession session) {

        User user = getUser(session);
        if (user != null) {
            String userImage = getImage(user);
            model.addAttribute("image", userImage);
        }


        List<Addon> addons;
        if (ide == null && name == null && sort == null) {
//            addons = addonService.getAll();
            addons = addonService.getAllApproved();

        } else {
            addons = addonService.filter(Optional.ofNullable(ide), Optional.ofNullable(name), Optional.ofNullable(sort));
        }
        model.addAttribute("addons", addons);

        List<Ide> ides = ideService.getAll();

        for (Addon addon : addons) {
            addon.setRoundedRate((int) Math.round(addon.getRating()));
            addon.setImageURI(getAddonImage(addon));
            addon.setRaters((int) addonRatingService.calculateRaters(addon.getId()));
            User addonCreator = addon.getUser();
            addonCreator.setImageURI(getImage(addonCreator));
        }

        model.addAttribute("user", user);
        model.addAttribute("ides", ides);

        return "pages/course-filter-list";
    }

    private User getUser(HttpSession session) {
        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return null;
        }
        return user;
    }

}
