package com.alpha33.addonis.controllers.mvc;

import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.*;
import com.alpha33.addonis.models.*;
import com.alpha33.addonis.services.contracts.*;
import com.alpha33.addonis.utils.AddonMapper;
import com.alpha33.addonis.utils.Utilities;
import com.alpha33.addonis.utils.ValidationHelper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/addons")
public class AddonMvcController {

    private final AddonService addonService;
    private final IdeService ideService;
    private final AddonMapper addonMapper;
    private final AuthenticationHelper authenticationHelper;
    private final TagService tagService;
    private final ValidationHelper validationHelper;
    private final AddonRatingService addonRatingService;
    private final UserService userService;
    private final GitHubService gitHubService;

    @Autowired
    public AddonMvcController(AddonService addonService, IdeService ideService,
                              AddonMapper addonMapper, AuthenticationHelper authenticationHelper,
                              TagService tagService, ValidationHelper validationHelper,
                              AddonRatingService addonRatingService, UserService userService, GitHubService gitHubService) {
        this.addonService = addonService;
        this.ideService = ideService;
        this.addonMapper = addonMapper;
        this.authenticationHelper = authenticationHelper;
        this.tagService = tagService;
        this.validationHelper = validationHelper;
        this.addonRatingService = addonRatingService;
        this.userService = userService;
        this.gitHubService = gitHubService;
    }

    @ModelAttribute("ides")
    public List<Ide> populateIdes() {
        return ideService.getAll();
    }

    @ModelAttribute("addons")
    public List<Addon> populateAddons() {
        return addonService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagService.getAll();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUserUsername") != null;
    }

//    to test quickly
//    @GetMapping("/dummymvc")
//    public String dummy(Model model, HttpSession session) {
//        return "/pages/200-action-successful";
//    }

    @GetMapping("/new")
    public String showNewAddonPage(Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            validationHelper.validateUserRights(user);
            model.addAttribute("user", user);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            model.addAttribute("addon", new AddonBodyDTO());
            return "pages/create-addon";
        } catch (AuthenticationFailureException e) {
            return "pages/sign-in";
        } catch (UnauthorizedOperationException e) {
            return "pages/403-forbidden";
        }
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String createAddon(@Valid @RequestParam("image_file") MultipartFile imageFile,
                              @Valid @RequestParam("content_file") MultipartFile contentFile,
                              @RequestParam("tags") String[] tags,
                              @Valid @ModelAttribute("addon") AddonBodyDTO addonBodyDTO, BindingResult errors,
                              Model model, HttpSession session) {

        if (errors.hasErrors()) {
            return "pages/400-bad-request";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            validationHelper.validateUserRights(user);
            Addon newAddon = addonService.prepareMvcNewAddon(addonBodyDTO, user, imageFile, contentFile, tags);
            addonService.create(newAddon, user);
            addonService.update(newAddon);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            return "pages/landings/addon-created";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("addonName", "duplicate_name", e.getMessage());
            return "pages/409-conflict-duplicate-entry";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/sign-in";
        } catch (IOException e) {
            e.printStackTrace();
            return "pages/404-error";
        }
    }

    @GetMapping("/{id}")
    public String showSingleAddon(@PathVariable int id, Model model, HttpSession session) {
        User user;
        if (authenticationHelper.isUserLogged(session)) {
            user = authenticationHelper.tryGetUser(session);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            model.addAttribute("loggedUser", user);
        }

        try {
            Addon addon = addonService.getById(id);
            if (addon.getStatus().toString().equalsIgnoreCase("PENDING")) {
                return "pages/404-error";
            }
            String[] sentences = addon.getDescription().split("\\.");
            addon.setRoundedRate((int) Math.round(addon.getRating()));
            addon.setRaters((int) addonRatingService.calculateRaters(addon.getId()));
            GitHubInfo gitHubInfo = gitHubService.getGitDetails(addon.getName());
            model.addAttribute("addon", addon);
            model.addAttribute("addonTags", addon.getTags().toArray());
            model.addAttribute("github", gitHubInfo);
            model.addAttribute("addonImage", Utilities.getAddonImage(addon));
            model.addAttribute("userImage", Utilities.getImage(addon.getUser()));
            model.addAttribute("user", addon.getUser());
            model.addAttribute("userAddons",
                    addonService.getAllByUser(addon.getUser().getUsername(), addon.getUser()).size());
            model.addAttribute("shortDesc", sentences[0]);
            List<Addon> addonListOfFour = addonService.getAllApproved().stream().limit(4).collect(Collectors.toList());
            addonService.setAddonCardData(addonListOfFour);
            model.addAttribute("addonsFour", addonListOfFour);
            String[] percents = addonRatingService.ratingsByUsers(id);
            model.addAttribute("percents", percents);
            return "pages/course-single";
        } catch (EntityNotFoundException | IOException | GitHubException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }

    @GetMapping("/image")
    public void getAddonImage(@RequestParam int id, HttpServletResponse response) throws IOException {
        Addon addon = addonService.getById(id);
        byte[] image = addon.getImage();
        InputStream in = new ByteArrayInputStream(image);
        IOUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void getAddonContent(@RequestParam int id, HttpServletResponse response) throws IOException {
        Addon addon = addonService.getById(id);
        byte[] content = addon.getContent();
        InputStream in = new ByteArrayInputStream(content);
        IOUtils.copy(in, response.getOutputStream());
        response.flushBuffer();
        addon.setDownloads(addon.getDownloads() + 1);
        addonService.update(addon);
    }

    @GetMapping("/rate")
    public String showRateAddonPage(@RequestParam int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            validationHelper.validateUserRights(user);
            Addon addon = addonService.getById(id);
            model.addAttribute("addon", addon);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            model.addAttribute("loggedUser", user);
            return "pages/rate-addon";
        } catch (AuthenticationFailureException e) {
            return "pages/401-unauthorized-access";
        } catch (UnauthorizedOperationException e) {
            return "pages/403-forbidden";
        }
    }

    @RequestMapping(value = "/rate", method = RequestMethod.POST)
    public void rateAddon(@RequestParam int id, HttpSession session, @RequestParam("rating") Double rate, HttpServletResponse response) throws IOException {
        try {
            User user = authenticationHelper.tryGetUser(session);
            validationHelper.validateUserRights(user);
            Addon addon = addonService.getById(id);
            addonRatingService.setAddonRating(addon, user, rate);
            addonService.update(addon);
            response.sendRedirect("/addons/" + id);
        } catch (AuthenticationFailureException | IOException e) {
            response.sendRedirect("/errors/401-unauthorized");
        } catch (DuplicateEntityException e) {
            response.sendRedirect("/addons/" + id);
        }
    }

    @GetMapping("/update")
    public String showAddonUpdatePage(@RequestParam int id, HttpSession session, Model model) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            Addon addon = addonService.getById(id);
            validationHelper.validateUserRights(user);
            validationHelper.validateUserAndCreator(addon, user);
            String image = userService.getImage(user);
            model.addAttribute("image", image);
            model.addAttribute("addon", addon);
            return "pages/update-addon";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "pages/401-unauthorized-access";
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateAddon(@Valid @RequestParam int id,
                            @Valid @RequestParam("content_file") MultipartFile contentFile,
                            HttpSession session,
                            @Valid @ModelAttribute("addonNewData") Addon addonNewData,
                            BindingResult errors,
                            HttpServletResponse response) throws IOException {

        try {
            User user = authenticationHelper.tryGetUser(session);
            Addon existingAddon = addonService.getById(id);
            validationHelper.validateUserRights(user);
            validationHelper.validateUserAndCreator(existingAddon, user);
            Addon updatedAddon = addonMapper.mapAddonNewData(existingAddon, addonNewData, contentFile);
            addonService.update(updatedAddon, user);
            response.sendRedirect("/addons/" + id);
        } catch (UnsupportedOperationException | AuthenticationFailureException | IOException e) {
            response.sendRedirect("/errors/401-unauthorized");
        } catch (DuplicateEntityException e) {
            response.sendRedirect("/errors/409-conflict");
        }
    }

    @GetMapping("/delete")
    public String deleteAddon(@RequestParam int id, HttpSession session) {
        try {
            Addon addon = addonService.getById(id);
            User user = authenticationHelper.tryGetUser(session);
            addonService.delete(addon.getId(), user);
            return "redirect:/";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            return "pages/401-unauthorized-access";
        }
    }
}