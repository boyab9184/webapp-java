package com.alpha33.addonis.controllers.mvc;


import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.AuthenticationFailureException;
import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.RegisterDto;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.models.UserProfileDto;
import com.alpha33.addonis.services.UserMapper;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.alpha33.addonis.utils.Utilities.getAddonImage;
import static com.alpha33.addonis.utils.Utilities.getImage;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AddonService addonService;
    private final AddonRatingService addonRatingService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;


    @Autowired
    public UserMvcController(UserService userService,
                             AddonService addonService,
                             AddonRatingService addonRatingService,
                             AuthenticationHelper authenticationHelper,
                             UserMapper userMapper) {
        this.userService = userService;
        this.addonService = addonService;
        this.addonRatingService = addonRatingService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;

    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUserUsername") != null;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getAllUsers(Model model,
                              @RequestParam(value = "search", required = false) String search,
                              HttpSession session) {

        User admin = getUser(session);
        if (admin == null) {
            return "redirect:/auth/login";
        }

        try {
            List<User> userList;
            if (search != null) {
                userList = userService.search(Optional.of(search), admin);
                model.addAttribute("users", userList);
            } else {
                userList = userService.getAll(admin);
            }
            model.addAttribute("users", userList);

            for (User user : userList) {
                user.setImageURI(getImage(user));
                int addonsNumber = addonService.getAllByUser(user.getUsername(), user).size();
                user.setAddonsNumber(addonsNumber);

                if (addonsNumber > 0) {
                    Date lastUploadDate = addonService.getAllByUser(user.getUsername(), user).get(0).getUploadDate();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM, yyyy");
                    String dateOnly = dateFormat.format(lastUploadDate);
                    user.setLastUploadDate(dateOnly);
                }
            }
            String image = getImage(admin);

            model.addAttribute("username", admin.getUsername());
            model.addAttribute("email", admin.getEmail());
            model.addAttribute("image", image);

            return "pages/dashboard/admin-students";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }

    @GetMapping("/dashboard")
    public String getUserAddons(Model model, HttpSession session) {

        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }
        String userImage = getImage(user);
        try {
            List<Addon> addons = addonService.getAllByUser(user.getUsername(), user);


            for (Addon addon : addons) {
                addon.setRoundedRate((int) Math.round(addon.getRating()));
                addon.setImageURI(getAddonImage(addon));
                addon.setRaters((int) addonRatingService.calculateRaters(addon.getId()));
            }
            model.addAttribute("addons", addons);
            model.addAttribute("user", user);
            model.addAttribute("image", userImage);


            return "pages/dashboard-student";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }


    @GetMapping("/{id}/block-unblock")
    public String blockUnblock(@PathVariable int id, Model model, HttpSession session) {
        User admin = getUser(session);

        if (admin == null) {
            return "redirect:/auth/login";
        }

        User user = userService.getById(admin, id);
        try {
            userService.updateUserStatus(user, admin);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }

    }


    @GetMapping("/update-profile")
    public String getUserUpdateProfilePage(Model model, HttpSession session) throws IOException {


        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }

        String image = getImage(user);
        UserProfileDto userProfileDto = userMapper.fromUserToDto(user);

        model.addAttribute("user", user);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("image", image);
        model.addAttribute("user1", userProfileDto);

        return "pages/profile-edit";
    }

    @PostMapping("/update-profile/photo")
    public String updatePhoto(@RequestParam(value = "image") MultipartFile imageFile, HttpSession session) {

        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }

        try {
            userService.updatePhoto(user, imageFile.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/";
    }


    @PostMapping("/update-profile")
    public String updateProfile(@Valid @ModelAttribute("user1") UserProfileDto userProfileDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {

        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "pages/profile-edit";
        }

        try {
            User userToUpdate = userMapper.fromDto(userProfileDto, user.getId());
            userService.updateProfile(userToUpdate);
            return "redirect:/";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_email", e.getMessage());
            return "pages/profile-edit";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }


    @GetMapping("/update-password")
    public String getUserUpdatePasswordPage(Model model, HttpSession session) {

        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }

        String image = getImage(user);
        RegisterDto registerDto = userMapper.fromUserToLoginDto(user);

        model.addAttribute("userLogged", user);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", registerDto);
        model.addAttribute("image", image);

        return "pages/security";
    }


    @PostMapping("/update-password")
    public String updatePassword(@Valid @ModelAttribute("user") RegisterDto registerDto,
                                 BindingResult errors, Model model, HttpSession session) {

        User user = getUser(session);
        if (user == null) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "pages/security";
        }


        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password.");
            return "pages/security";
        }

        try {
            User userToUpdate = userMapper.fromDto(registerDto, user.getId());
            userService.updatePassword(userToUpdate);
            session.removeAttribute("currentUserUsername");
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }


    private User getUser(HttpSession session) {
        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return null;
        }
        return user;
    }


}
