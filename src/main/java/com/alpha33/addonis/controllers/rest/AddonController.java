package com.alpha33.addonis.controllers.rest;

import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.AddonBodyDTO;
import com.alpha33.addonis.models.AddonResponseView;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.AddonRatingRepository;
import com.alpha33.addonis.repositories.contracts.UserRepository;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.utils.AddonMapper;
import com.alpha33.addonis.utils.Utilities;
import com.alpha33.addonis.utils.ValidationHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/addonis/addons")
public class AddonController {

    private final AddonService addonService;
    private final AuthenticationHelper authenticationHelper;
    private final AddonMapper addonMapper;
    private final ValidationHelper validationHelper;
    private final UserRepository userRepository;
    private final AddonRatingRepository addonRatingRepository;
    private final AddonRatingService addonRatingService;

    @Autowired
    public AddonController(AddonService addonService, AuthenticationHelper authenticationHelper,
                           AddonMapper addonMapper, ValidationHelper validationHelper,
                           UserRepository userRepository, AddonRatingRepository addonRatingRepository,
                           AddonRatingService addonRatingService) {
        this.addonService = addonService;
        this.authenticationHelper = authenticationHelper;
        this.addonMapper = addonMapper;
        this.validationHelper = validationHelper;
        this.userRepository = userRepository;
        this.addonRatingRepository = addonRatingRepository;
        this.addonRatingService = addonRatingService;
    }

    //for quickly testing
    @GetMapping(value = "/dummy/{id}")
    public String dummy(@PathVariable int id) {
        double avg = addonRatingRepository.calculateRating(id);
        System.out.println(avg);
        return String.valueOf(avg);
    }

    @GetMapping
    public List<AddonResponseView> getAll() {
        return addonMapper.convertAddonsListToAddonsResponseViewList(addonService.getAll());
    }

    @GetMapping("/user/{id}")
    public List<AddonResponseView> getAllForUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);

        try {
            User searchedUser = userRepository.getById(id);
            return addonMapper.convertAddonsListToAddonsResponseViewList(
                    addonService.getAllByUser(searchedUser.getUsername(), user));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<AddonResponseView> filter(@RequestParam(required = false) Optional<String> ide,
                                          @RequestParam(required = false) Optional<String> name,
                                          @RequestParam(required = false) Optional<String> sort) {

        return addonMapper.convertAddonsListToAddonsResponseViewList(addonService.filter(ide, name, sort));
    }

    @GetMapping("/popular")
    @ApiOperation(value = "List the first 5 addons with most downloads")
    public List<AddonResponseView> getMostPopular() {
        return addonMapper.convertAddonsListToAddonsResponseViewList(addonService.getMostPopular());
    }

    @GetMapping("/new")
    @ApiOperation(value = "List the first 5 addons with most recent upload date")
    public List<AddonResponseView> getNew() {
        return addonMapper.convertAddonsListToAddonsResponseViewList(addonService.getNew());
    }

    @GetMapping("/featured")
    @ApiOperation(value = "List the first 5 addons chosen from the admin")
    public List<AddonResponseView> getFeatured() {
        return addonMapper.convertAddonsListToAddonsResponseViewList(addonService.getFeatured());
    }

    @GetMapping("download/{id}")
    public String downloadContent(@RequestHeader HttpHeaders headers, @PathVariable int id) throws IOException {

        Addon addonToBeDownloaded;
        try {
            addonToBeDownloaded = addonService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        addonToBeDownloaded.setDownloads(addonToBeDownloaded.getDownloads() + 1);
        addonService.update(addonToBeDownloaded);

        return "{\"downloadUrl\": \"Http://" + headers.getHost() + "/addons/download?id=" + id + "\"}";
    }

    @PostMapping
    public Addon create(@RequestHeader HttpHeaders headers,
                        @Valid @RequestParam("json") String json, @RequestPart("image") MultipartFile imageFile,
                        @RequestPart("content") MultipartFile contentFile,
                        @RequestParam("tags") String tagName) {

        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);

        if ((imageFile.isEmpty()) || (contentFile.isEmpty())) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }

        Addon newAddon = addonService.prepareNewAddon(json, contentFile, imageFile, tagName, user);
        try {
            addonService.create(newAddon, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        addonService.update(newAddon, user);
        return newAddon;
    }

    @PostMapping("/rate/{id}")
    @ApiOperation(value = "Authenticated user can rate an addon")
    public AddonResponseView rateAddon(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                       @RequestParam("rating") Double rate) {

        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);
        Addon addon;

        try {
            addon = addonService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        addonRatingService.setAddonRating(addon, user, rate);
        addonService.update(addon, user);
        return addonMapper.convertToAddonResponseView(addon);
    }

    @PatchMapping("update/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestHeader HttpHeaders headers, @RequestBody JsonPatch patch, @PathVariable int id) {

        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);

        try {
            Addon addonToBeUpdated = addonService.getById(id);
            validationHelper.validateUserAndCreator(addonToBeUpdated, user);

            Addon addon = addonService.applyPatchToAddon(patch, addonToBeUpdated);
            addonService.setAddonBinaryAndUserData(addon, addonToBeUpdated);
            addonService.update(addon, user);
        } catch (JsonPatchException | JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PatchMapping("/highlight/{id}")
    @ApiOperation(value = "Admin can mark an addon as highlighted")
    public void highlightAddon(@RequestHeader HttpHeaders headers, @RequestBody JsonPatch patch, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeAdmin(user);
        try {
            Addon addonToBeUpdated = addonService.getById(id);
            Addon addon = addonService.applyPatchToAddon(patch, addonToBeUpdated);
            addonService.setAddonBinaryAndUserData(addon, addonToBeUpdated);
            addonService.update(addon, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (JsonPatchException | JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public Addon fullUpdate(@RequestHeader HttpHeaders headers,
                            @RequestParam("json") String json, @RequestPart("image") MultipartFile imageFile,
                            @RequestPart("content") MultipartFile contentFile,
                            @PathVariable int id) {

        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);
        Addon addon;
        try {
            validationHelper.validateUserAndCreator(addonService.getById(id), user);
            addon = addonService.getById(id);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        AddonBodyDTO addonBodyDTO = addonService.applyPatchToAddonBodyDto(json);
        addonBodyDTO.setFileName(contentFile.getOriginalFilename());
        Addon newAddon = addonMapper.convertAddonDtoToAddon(addonBodyDTO, addon.getId(),
                Utilities.getFileBytes(imageFile), Utilities.getFileBytes(contentFile));

        addonService.update(newAddon, user);
        return newAddon;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        authenticationHelper.authorizeUser(user);
        Addon addon;

        try {
            addon = addonService.getById(id);
            addonService.delete(addon.getId(), user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
