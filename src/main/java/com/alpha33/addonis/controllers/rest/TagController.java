package com.alpha33.addonis.controllers.rest;

import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.TagRepository;
import com.alpha33.addonis.services.contracts.AddonService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/addonis/tags")
public class TagController {

    private final AuthenticationHelper authenticationHelper;
    private final AddonService addonService;
    private final TagRepository tagRepository;

    public TagController(AuthenticationHelper authenticationHelper, AddonService addonService, TagRepository tagRepository) {
        this.authenticationHelper = authenticationHelper;
        this.addonService = addonService;
        this.tagRepository = tagRepository;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("/create/{addonId}")
    public void create(@RequestHeader HttpHeaders headers, @Valid @RequestBody Tag tag, @PathVariable int addonId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Tag> foundTags = tagRepository.getByName(tag.getName());
            Tag tagToBeAdded;
            if (foundTags.isEmpty()) {
                tagRepository.create(tag);
                tagToBeAdded = tag;
            } else {
                //tags are unique, so we should not have more than 1 tag found? TODO: review design eventually
                tagToBeAdded = foundTags.get(0);
            }

            Addon addon = addonService.getById(addonId);
            addon.getTags().add(tagToBeAdded);
            addonService.update(addon, user);

        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
