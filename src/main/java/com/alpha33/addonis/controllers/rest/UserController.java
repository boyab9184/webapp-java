package com.alpha33.addonis.controllers.rest;


import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.models.UserDto;
import com.alpha33.addonis.models.UserStatusDto;
import com.alpha33.addonis.models.UserUpdateDto;
import com.alpha33.addonis.services.UserMapper;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/addonis/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> search) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return userService.search(search, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        User user = authenticationHelper.tryGetUser(headers);
        try {
            return userService.getById(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        try {
            userService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody UserDto userDto) {

        try {
            User user = userMapper.fromDto(userDto);
            userService.create(user);

            return user;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("details/{id}")
    public User updateUserDetails(@PathVariable int id,
                                  @RequestBody UserUpdateDto userUpdateDto,
                                  @RequestHeader HttpHeaders headers) {

        try {

            User userAuthenticated = authenticationHelper.tryGetUser(headers);

            User user = userMapper.fromDto(userUpdateDto, id);

            userService.update(user, userAuthenticated);

            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());
        }
    }

    @PutMapping("photo")
    public User updateUserPhoto(@RequestPart("photo") MultipartFile imageFile,
                                @RequestHeader HttpHeaders headers) throws IOException {


        try {

            User userAuthenticated = authenticationHelper.tryGetUser(headers);

            userService.updatePhoto(userAuthenticated, imageFile.getBytes());

            return userAuthenticated;

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());

        }
    }

    @PutMapping("status/{id}")

    public User updateUserStatus(@PathVariable int id,
                                 @RequestBody UserStatusDto userStatusDto,
                                 @RequestHeader HttpHeaders headers) {


        try {

            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            User user = userMapper.fromDto(userStatusDto, id);

            userService.updateUserStatus(user, authenticatedUser);

            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());
        }

    }


}



