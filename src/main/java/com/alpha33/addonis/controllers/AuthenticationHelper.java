package com.alpha33.addonis.controllers;

import com.alpha33.addonis.exceptions.AuthenticationFailureException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_EMAIL = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong email or password";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_EMAIL)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested resource requires authentication!");
        }

        try {
            String userEmail = headers.getFirst(AUTHORIZATION_HEADER_EMAIL);
            return userService.getByEmail(userEmail);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid Email");
        }
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUserUsername");
        if (currentUserUsername == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }
        return userService.getByUsername(currentUserUsername);
    }

    public void authorizeUser(User user) {
        if (user.isBlocked()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is blocked.");
        }
    }

    public void authorizeAdmin(User user) {
        if (!user.isAdmin()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not admin.");
        }
    }

    public boolean isUserLogged(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUserUsername");
        return (currentUserUsername != null);
    }
}
