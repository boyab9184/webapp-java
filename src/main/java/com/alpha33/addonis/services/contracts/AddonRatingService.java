package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;

public interface AddonRatingService {

    long calculateRaters(int id);

    void setAddonRating(Addon addon, User user, double rateValue);

    String[] ratingsByUsers(int id);
}
