package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.Tag;

import java.util.List;

public interface TagService {

    void create(Tag tag);

    void createNew(String tagName);

    String[] getTags(String tag);

    List<Tag> createNotPresentTags(String[] tags);

    //String getSingleTag(String input);

    String[] getMvcTags(String[] tag);

    List<Tag> getAll();
}
