package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.AddonBodyDTO;
import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;

public interface AddonService {

    List<Addon> getAll();

    Addon getById(int id);

    List<Addon> getAllByUser(String username, User user);

    void create(Addon addon, User user);

    Addon applyPatchToAddon(JsonPatch patch, Addon addon) throws JsonPatchException, JsonProcessingException;

    AddonBodyDTO applyPatchToAddonBodyDto(String json);

    void update(Addon addon, User user);

    void delete(int id, User user);

    Addon addAllTagsToAddon(List<Tag> list, Addon addon);

    List<Addon> filter(Optional<String> ide, Optional<String> name, Optional<String> sort);

    List<Addon> getMostPopular();

    List<Addon> getNew();

    List<Addon> getFeatured();

    void setAddonBinaryAndUserData(Addon addon, Addon existingAddon);

    ZipEntry downloadContent(String fileName) throws IOException;

    Addon prepareNewAddon(String json, MultipartFile contentFile, MultipartFile imageFile, String tagName, User user);

    Addon prepareMvcNewAddon(AddonBodyDTO addonBodyDTO, User user, MultipartFile image_file, MultipartFile content_file, String[] tags) throws IOException;

    void update(Addon addon);

    List<Addon> getAllApproved();

    void setAddonCardData(List<Addon> list);
}
