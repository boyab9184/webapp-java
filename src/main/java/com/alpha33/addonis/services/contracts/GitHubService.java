package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.GitHubInfo;

import java.io.IOException;

public interface GitHubService {

    GitHubInfo getGitDetails(String link) throws IOException;
}
