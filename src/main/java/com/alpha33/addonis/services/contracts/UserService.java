package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll(User user);

    User getById(User user, int id);

    void delete(User user, int id);

    void create(User user);

    void update(User user, User userAuthenticated);

    List<User> search(Optional<String> search, User user);

    void updateUserStatus(User user, User authenticatedUser);

    int getUserStatus(int id, User user);

    User getByEmail(String email);

    User getByUsername(String username);

    User getByPhone(String phone);

    void updatePhoto(User userAuthenticated, byte[] imageFile);

    void updateProfile(User user);

    void updatePassword(User user);

    void updateAddonStatus(Addon addon, User admin);

    void updateAddonFeatured(Addon addon, User admin);

    String getImage(User user);

}
