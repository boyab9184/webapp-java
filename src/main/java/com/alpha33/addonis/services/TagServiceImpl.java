package com.alpha33.addonis.services;

import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.repositories.contracts.TagRepository;
import com.alpha33.addonis.services.contracts.TagService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void create(Tag tag) {
        List<Tag> foundTags = tagRepository.getByName(tag.getName());
        if (foundTags.isEmpty()) {
            tagRepository.create(tag);
        }
    }

    @Override
    public void createNew(String tagName) {
        List<Tag> foundTags = tagRepository.getByName(tagName);
        if (foundTags.isEmpty()) {
            Tag newTag = new Tag();
            newTag.setName(tagName.substring(0, 1).toUpperCase() + tagName.substring(1).toLowerCase());
            tagRepository.create(newTag);
        }
    }

    @Override
    public String[] getTags(String tag) {
        return tag.split(",");
    }

    @Override
    public List<Tag> createNotPresentTags(String[] tags) {
        List<Tag> currentTags = new ArrayList<>();
        for (String s : tags) {
            if (tagRepository.getByNameNew(s) == null) {
                createNew(s);
            }
            currentTags.add(tagRepository.getByNameNew(s));
        }
        return currentTags;
    }

    @Override
    public String[] getMvcTags(String[] tag) {
        String[] newList = new String[tag.length];
        int j = 0;
        while (j < newList.length) {
            newList[j] = getSingleTag(tag[j]);
            j++;
        }
        return newList;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    private String getSingleTag(String tag) {
        String[] value = tag.split(":");
        String singleTag = value[1];
        StringBuilder actualTag = new StringBuilder();
        int i = 0;
        while (i < singleTag.length()) {
            char singleChar = singleTag.charAt(i);
            if ((singleChar != 34) && (singleChar != 93) && (singleChar != 125)) {
                actualTag.append(singleChar);
            }
            i++;
        }
        return actualTag.toString();
    }
}
