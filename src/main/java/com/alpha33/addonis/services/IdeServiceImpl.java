package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.Ide;
import com.alpha33.addonis.repositories.contracts.IdeRepository;
import com.alpha33.addonis.services.contracts.IdeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IdeServiceImpl implements IdeService {

    private final IdeRepository ideRepository;

    public IdeServiceImpl(IdeRepository ideRepository) {
        this.ideRepository = ideRepository;
    }

    @Override
    public List<Ide> getAll() {
        return ideRepository.getAll();
    }

    @Override
    public void create(Ide ide) {
        boolean duplicateExists = true;
        try {
            ideRepository.getByName(ide.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Ide", "name", ide.getName());
        }
        ideRepository.create(ide);
    }
}
