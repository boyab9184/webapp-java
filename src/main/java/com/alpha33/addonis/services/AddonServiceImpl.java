package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.AddonBodyDTO;
import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.services.contracts.TagService;
import com.alpha33.addonis.services.contracts.UserService;
import com.alpha33.addonis.utils.AddonMapper;
import com.alpha33.addonis.utils.Utilities;
import com.alpha33.addonis.utils.ValidationHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class AddonServiceImpl implements AddonService {

    private final AddonRepository addonRepository;
    private final AddonMapper addonMapper;
    private final ObjectMapper objectMapper;
    private final TagService tagService;
    private final ValidationHelper validationHelper;
    private final AddonRatingService addonRatingService;
    private final UserService userService;
    public static final String GET_ALL_ADDONS_FOR_USER_ERROR_MESSAGE = "Only owner or admin can see all addons for a specific user.";

    @Autowired
    public AddonServiceImpl(AddonRepository addonRepository, AddonMapper addonMapper,
                            ObjectMapper objectMapper, TagService tagService, ValidationHelper validationHelper,
                            AddonRatingService addonRatingService, UserService userService) {
        this.addonRepository = addonRepository;
        this.addonMapper = addonMapper;
        this.objectMapper = objectMapper;
        this.tagService = tagService;
        this.validationHelper = validationHelper;
        this.addonRatingService = addonRatingService;
        this.userService = userService;
    }

    @Override
    public List<Addon> getAll() {
        return addonRepository.getAll();
    }

    @Override
    public Addon getById(int id) {
        return addonRepository.getById(id);
    }

    @Override
    public List<Addon> getAllByUser(String username, User authenticatedUser) {
        if ((!authenticatedUser.isAdmin()) && (!authenticatedUser.getUsername().equals(username))) {
            throw new UnauthorizedOperationException(GET_ALL_ADDONS_FOR_USER_ERROR_MESSAGE);
        }
        return addonRepository.getAllByUser(username);
    }

    @Override
    public void create(Addon addon, User user) {
        try {
            validationHelper.validateUserRights(user);
        } catch (UnauthorizedOperationException e) {
            throw new UnauthorizedOperationException(e.getMessage());
        }

        boolean duplicateNameExists = true;
        boolean duplicateFileNameExists = true;

        try {
            addonRepository.getByName(addon.getName());
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }
        if (duplicateNameExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        try {
            addonRepository.getByFileName(addon.getFileName());
        } catch (EntityNotFoundException e) {
            duplicateFileNameExists = false;
        }
        if (duplicateFileNameExists) {
            throw new DuplicateEntityException("Addon", "fileName", addon.getFileName());
        }

        addonRepository.create(addon);
    }

    @Override
    public Addon applyPatchToAddon(JsonPatch patch, Addon targetAddon) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(targetAddon, JsonNode.class));
        return objectMapper.treeToValue(patched, Addon.class);
    }

    @Override
    public AddonBodyDTO applyPatchToAddonBodyDto(String json) {
        AddonBodyDTO addonBodyDTO = new AddonBodyDTO();
        try {
            addonBodyDTO = objectMapper.readValue(json, AddonBodyDTO.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return addonBodyDTO;
    }

    @Override
    public void update(Addon addon, User user) {

        try {
            validationHelper.validateUserRights(user);
            validationHelper.validateUserAndCreator(addon, user);
        } catch (UnauthorizedOperationException e) {
            throw new UnauthorizedOperationException(e.getMessage());
        }

        boolean duplicateNameExists = true;
        boolean duplicateFileNameExists = true;

        try {
            Addon existingAddon = addonRepository.getByName(addon.getName());
            if (Objects.equals(existingAddon.getId(), addon.getId())) {
                duplicateNameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }
        if (duplicateNameExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        try {
            Addon existingAddon = addonRepository.getByFileName(addon.getFileName());
            if (Objects.equals(existingAddon.getId(), addon.getId())) {
                duplicateFileNameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateFileNameExists = false;
        }
        if (duplicateFileNameExists) {
            throw new DuplicateEntityException("Addon", "fileName", addon.getFileName());
        }

        addonRepository.update(addon);
    }

    @Override
    public void delete(int id, User user) {
        try {
            Addon addon = getById(id);
            validationHelper.validateUserRights(user);
            validationHelper.validateUserAndCreator(addon, user);
            addonRepository.delete(id);
        } catch (UnauthorizedOperationException e) {
            throw new UnauthorizedOperationException(e.getMessage());
        }
    }

    @Override
    public Addon addAllTagsToAddon(List<Tag> list, Addon addon) {
        for (Tag t : list) {
            addon.getTags().add(t);
        }
        return addon;
    }

    @Override
    public List<Addon> getMostPopular() {
        return addonRepository.getMostPopular();
    }

    @Override
    public List<Addon> getNew() {
        return addonRepository.getNew();
    }

    @Override
    public List<Addon> getFeatured() {
        return addonRepository.getFeatured();
    }

    @Override
    public List<Addon> filter(Optional<String> ide, Optional<String> name, Optional<String> sort) {
        return addonRepository.filter(ide, name, sort);
    }

    @Override
    public ZipEntry downloadContent(String fileName) throws IOException {

        ZipInputStream zipStream = new ZipInputStream
                (new ByteArrayInputStream(addonRepository.getByFileName(fileName).getContent()));

        return zipStream.getNextEntry();
    }

    @Override
    public void setAddonBinaryAndUserData(Addon addon, Addon existingAddon) {
        addon.setContent(existingAddon.getContent());
        addon.setImage(existingAddon.getImage());
        addon.setUser(existingAddon.getUser());
    }

    @Override
    public Addon prepareNewAddon(String json, MultipartFile contentFile, MultipartFile imageFile, String tagName, User user) {
        AddonBodyDTO addonBodyDTO = applyPatchToAddonBodyDto(json);
        addonBodyDTO.setFileName(contentFile.getOriginalFilename());
        List<Tag> inputTags = tagService.createNotPresentTags(tagService.getTags(tagName));
        Addon newAddon = addonMapper.convertAddonDtoToAddon(addonBodyDTO, user, Utilities.getFileBytes(imageFile), Utilities.getFileBytes(contentFile));
        newAddon = addAllTagsToAddon(inputTags, newAddon);
        return newAddon;
    }

    @Override
    public Addon prepareMvcNewAddon(AddonBodyDTO addonBodyDTO, User user, MultipartFile image_file, MultipartFile content_file, String[] tags) throws IOException {
        Addon addon;
        try {
            addon = addonMapper.convertAddonDtoToAddon(addonBodyDTO, user, image_file.getBytes(), content_file.getBytes());
            addon.setFileName(content_file.getOriginalFilename());
            List<Tag> list = tagService.createNotPresentTags(tagService.getMvcTags(tags));
            addAllTagsToAddon(list, addon);
            return addon;
        } catch (IOException e) {
            throw new IOException();
        }
    }

    @Override
    public void update(Addon addon) {
        addonRepository.update(addon);
    }

    @Override
    public List<Addon> getAllApproved() {
        return addonRepository.getAllApproved();
    }

    @Override
    public void setAddonCardData(List<Addon> list) {
        for (Addon addon : list) {
            addon.setRoundedRate((int) Math.round(addon.getRating()));
            addon.setRaters((int) addonRatingService.calculateRaters(addon.getId()));
            User addonCreator = addon.getUser();
            addonCreator.setImageURI(userService.getImage(addonCreator));
        }
    }
}
