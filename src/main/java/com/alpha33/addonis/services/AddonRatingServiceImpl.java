package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.AddonRating;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.AddonRatingRepository;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AddonRatingServiceImpl implements AddonRatingService {

    private final AddonRatingRepository addonRatingRepository;

    public AddonRatingServiceImpl(AddonRatingRepository addonRatingRepository) {
        this.addonRatingRepository = addonRatingRepository;
    }

    @Override
    public long calculateRaters(int id) {
        return addonRatingRepository.calculateRaters(id);
    }

    @Override
    public void setAddonRating(Addon addon, User user, double rate) {
        List<AddonRating> list = addonRatingRepository.getAll();
        if (!hasRated(list, user, addon)) {
            AddonRating addonRating = new AddonRating();
            addonRating.setAddon(addon);
            addonRating.setUser(user);
            addonRating.setRatingValue(rate);
            addonRatingRepository.create(addonRating);
            addon.setRating(addonRatingRepository.calculateRating(addon.getId()));
        } else {
            throw new DuplicateEntityException("Addon", "is already rated from user with username", user.getUsername());
        }
    }

    private boolean hasRated(List<AddonRating> list, User user, Addon ratedAddon) {

        int counter = 0;
        for (AddonRating addonRating : list) {
            if ((addonRating.getUser().getId() == user.getId()) &&
                    (Objects.equals(addonRating.getAddon().getId(), ratedAddon.getId()))) {
                counter++;
            }
        }
        return counter > 0;
    }

    @Override
    public String[] ratingsByUsers(int id) {
        long[] ratings = addonRatingRepository.ratingsByUsers(id);
        long totalRaters = calculateRaters(id);

        double oneStar = 0;
        double twoStars = 0;
        double threeStars = 0;
        double fourStars = 0;
        double fiveStars = 0;

        if (totalRaters > 0) {
            oneStar = ((double) ratings[0] / (double) totalRaters) * 100;
            twoStars = ((double) ratings[1] / (double) totalRaters) * 100;
            threeStars = ((double) ratings[2] / (double) totalRaters) * 100;
            fourStars = ((double) ratings[3] / (double) totalRaters) * 100;
            fiveStars = ((double) ratings[4] / (double) totalRaters) * 100;
        }

        return new String[]{String.format("%.0f", oneStar),
                String.format("%.0f", twoStars),
                String.format("%.0f", threeStars),
                String.format("%.0f", fourStars),
                String.format("%.0f", fiveStars)};
    }
}
