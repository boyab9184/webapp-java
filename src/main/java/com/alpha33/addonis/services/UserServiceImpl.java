package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import com.alpha33.addonis.repositories.contracts.UserRepository;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    public static final String ONLY_ADMIN_IS_ALLOWED_FOR_THIS_OPERATION = "Only admin is allowed for this operation";


    private final UserRepository userRepository;
    private final AddonRepository addonRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, AddonRepository addonRepository) {
        this.userRepository = userRepository;
        this.addonRepository = addonRepository;
    }

    @Override
    public List<User> getAll(User user) {
        isAdmin(user);
        return userRepository.getAll();
    }

    @Override
    public User getById(User user, int id) {
        isAdmin(user);
        return userRepository.getById(id);
    }

    @Override
    public void delete(User user, int id) {
        isAdmin(user);
        userRepository.delete(id);
    }


    @Override
    public void create(User user) {


        boolean usernameExists = true;
        boolean phoneExists = true;
        boolean emailExists = true;

        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            usernameExists = false;
        }

        if (usernameExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }

        if (emailExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }


        try {
            userRepository.getByPhone(user.getPhone());
        } catch (EntityNotFoundException e) {
            phoneExists = false;
        }

        if (phoneExists) {
            throw new DuplicateEntityException("User", "phone", user.getPhone());
        }

        userRepository.create(user);

    }


    @Override
    public void update(User user, User userAuthenticated) {

        if (!user.getUsername().equalsIgnoreCase(userAuthenticated.getUsername())) {
            throw new UnauthorizedOperationException(String
                    .format("User with username %s can update only its own data", userAuthenticated.getUsername()));
        }
        duplicateExits(user);

        userRepository.update(user);
    }

    @Override
    public void updateProfile(User user) {

        duplicateExits(user);

        userRepository.update(user);
    }

    public void updatePassword(User user) {
        userRepository.update(user);
    }


    @Override
    public void updatePhoto(User userAuthenticated, byte[] imageFile) {

        userAuthenticated.setImage(imageFile);
        userRepository.update(userAuthenticated);
    }

    @Override
    public List<User> search(Optional<String> search, User user) {

        isAdmin(user);

        if (search.isEmpty()) {
            return userRepository.getAll();
        } else {
            return userRepository.search(search.get());
        }
    }


    @Override
    public void updateUserStatus(User user, User authenticatedUser) {
        isAdmin(authenticatedUser);
        userRepository.updateUserStatus(user.getId());
    }

    @Override
    public void updateAddonStatus(Addon addon, User admin) {
        isAdmin(admin);
        addonRepository.update(addon);
    }

    @Override
    public void updateAddonFeatured(Addon addon, User admin) {
        isAdmin(admin);
        addonRepository.update(addon);
    }

    @Override
    public int getUserStatus(int id, User user) {
        isAdmin(user);
        return userRepository.getUserStatus(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }


    @Override
    public User getByPhone(String phone) {
        return userRepository.getByPhone(phone);
    }


    private void isAdmin(User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_IS_ALLOWED_FOR_THIS_OPERATION);
        }
    }


    private void duplicateExits(User user) {
        boolean emailExists = true;
        boolean phoneExists = true;

        try {
            User existingUser = userRepository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                emailExists = false;
            }
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }

        if (emailExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }


        try {
            User existingUser = userRepository.getByPhone(user.getPhone());
            if (existingUser.getId() == user.getId()) {
                phoneExists = false;
            }
        } catch (EntityNotFoundException e) {
            phoneExists = false;
        }

        if (phoneExists) {
            throw new DuplicateEntityException("User", "phone", user.getPhone());
        }
    }

    @Override
    public String getImage(User user) {
        String image;
        if (user.getImage() == null) {
            image = null;
        } else {
            image = "data:image/png;base64," + Base64.getEncoder().encodeToString(user.getImage());
        }
        return image;
    }
}
