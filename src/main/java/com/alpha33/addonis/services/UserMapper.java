package com.alpha33.addonis.services;


import com.alpha33.addonis.models.*;
import com.alpha33.addonis.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserRepository userRepository;


    @Autowired
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDto) {

        User user = new User();
        dtoToUserCreate(userDto, user);

        return user;
    }

    public User fromDto(UserUpdateDto userUpdateDto, int id) {

        User user = userRepository.getById(id);
        dtoToUserUpdate(userUpdateDto, user);

        return user;
    }

    public User fromDto(RegisterDto registerDto, int id) {

        User user = userRepository.getById(id);

        dtoToUserUpdatePassword(registerDto, user);

        return user;
    }

    public User fromDto(UserProfileDto userProfileDto, int id) {

        User user = userRepository.getById(id);
        dtoToUserUpdateProfile(userProfileDto, user);

        return user;
    }

    public User fromDto(UserStatusDto userStatusDto, int id) {

        User user = userRepository.getById(id);
        user.setStatus(userStatusDto.getStatus());

        return user;
    }

    public User fromDto(User user, byte[] image) {

        dtoToUserUpdatePhoto(user, image);

        return user;
    }

    private void dtoToUserCreate(UserDto userDto, User user) {

        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setPassword(userDto.getPassword());
    }

    private void dtoToUserUpdate(UserUpdateDto userUpdateDto, User user) {

        user.setEmail(userUpdateDto.getEmail());
        user.setPhone(userUpdateDto.getPhone());
//        user.setPassword(userUpdateDto.getPassword());

//       user.setImage();
    }

    private void dtoToUserUpdatePhoto(User user, byte[] image) {

//        user.setEmail(userUpdateDto.getEmail());
//        user.setPhone(userUpdateDto.getPhone());
//        user.setPassword(userUpdateDto.getPassword());

        user.setImage(image);
    }

    private void dtoToUserUpdateProfile(UserProfileDto userProfileDto, User user) {
        user.setEmail(userProfileDto.getEmail());
        user.setPhone(userProfileDto.getPhone());
    }

    private void dtoToUserUpdatePassword(RegisterDto registerDto, User user) {
        user.setPassword(registerDto.getPassword());
    }

    public UserProfileDto fromUserToDto(User user) {
        UserProfileDto userProfileDto = new UserProfileDto();

        userProfileDto.setEmail(user.getEmail());
        userProfileDto.setPhone(user.getPhone());

        return userProfileDto;
    }

    public RegisterDto fromUserToLoginDto(User user) {
        RegisterDto registerDto = new RegisterDto();

        registerDto.setPassword(user.getPassword());
//        registerDto.setUsername(user.getUsername());
//        registerDto.setEmail(user.getEmail());

        return registerDto;
    }


}
