package com.alpha33.addonis.models;

import com.alpha33.addonis.validators.ValidPassword;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class LoginDto {

    @NotBlank(message = "Username can't be empty")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols")
    private String username;

    @ValidPassword
    private String password;


    public LoginDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
