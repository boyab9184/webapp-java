package com.alpha33.addonis.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class AddonBodyDTO {

    public static final String IDE_ID_INPUT_ERROR = "IdeId must be positive";
    public static final String ADDON_NAME_LENGTH_CONSTRAINTS_ERROR = "AddonName must be in range of 3 and 30 characters long.";
    public static final String LINK_NAME_LENGTH_CONSTRAINTS_ERROR = "Link can be up to 300 characters long.";
    public static final String FILE_NAME_LENGTH_CONSTRAINTS_ERROR = "FileName can be up to 30 characters long.";
    public static final String ORIGIN_LINK_PATTERN_ERROR = "Origin Link must come from a GitHub repository.";
    public static final String DESCRIPTION_LENGTH_CONSTRAINTS_ERROR = "Description must be at least 50 characters long";


    @Size(min = 3, max = 30, message = ADDON_NAME_LENGTH_CONSTRAINTS_ERROR)
    @NotNull
    public String addonName;

    @NotNull
    @Positive(message = IDE_ID_INPUT_ERROR)
    private Integer ideId;

    @Size(min = 20, max = 300, message = LINK_NAME_LENGTH_CONSTRAINTS_ERROR)
//    @Pattern(regexp = "https:\\/\\/github\\.com\\/.+\\/.+",
//            message = ORIGIN_LINK_PATTERN_ERROR)
    @NotNull
    private String originLocation;

    @Size(min = 50, message = DESCRIPTION_LENGTH_CONSTRAINTS_ERROR)
    @NotNull
    private String description;

    @Size(min = 2, max = 150, message = FILE_NAME_LENGTH_CONSTRAINTS_ERROR)
    private String fileName;

    public AddonBodyDTO() {
    }

    public String getAddonName() {
        return addonName;
    }

    public void setAddonName(String addonName) {
        this.addonName = addonName;
    }

    public Integer getIdeId() {
        return ideId;
    }

    public void setIdeId(Integer ideId) {
        this.ideId = ideId;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return addonName + " " + description;
    }

}
