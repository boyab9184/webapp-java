package com.alpha33.addonis.models;

public class AddonResponseView {

    String addonId;
    String addonName;
    String ideName;
    String originLocation;
    String status;
    String downloads;
    String description;
    String rating;
    String uploadDate;
    String username;
    String fileName;

    public AddonResponseView() {
    }

    public AddonResponseView(String addonId, String addonName, String ideName, String originLocation, String status, String downloads,
                             String description, String rating, String uploadDate, String username, String fileName) {
        this.addonId = addonId;
        this.addonName = addonName;
        this.ideName = ideName;
        this.originLocation = originLocation;
        this.status = status;
        this.downloads = downloads;
        this.description = description;
        this.rating = rating;
        this.uploadDate = uploadDate;
        this.username = username;
        this.fileName = fileName;
    }

    public String getAddonId() {
        return addonId;
    }

    public void setAddonId(String addonId) {
        this.addonId = addonId;
    }

    public String getAddonName() {
        return addonName;
    }

    public void setAddonName(String addonName) {
        this.addonName = addonName;
    }

    public String getIdeName() {
        return ideName;
    }

    public void setIdeName(String ideName) {
        this.ideName = ideName;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
