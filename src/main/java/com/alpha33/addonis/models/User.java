package com.alpha33.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "phone_number")
    private String phone;

    @JsonIgnore
    @Lob
    @Column(name = "image", columnDefinition = "BLOB")
    private byte[] image;

    @Column(name = "status")
    private int status = 1;


    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private Set<Role> roles = new HashSet<>();


    @JsonIgnore
//    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Addon> addons = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
    private List<AddonRating> addonRatingsList;

    @Transient
    private String imageURI;

    @Transient
    private int addonsNumber;

    @Transient
    private String lastUploadDate;


    public User(int id,
                String username,
                String email,
                String password,
                String phone,
                int status,
                Set<Role> roles,
                Set<Addon> addons) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.status = status;
        this.roles = roles;
        this.addons = addons;

        this.roles.add(new Role(2, "User"));
    }


    public User() {

        roles.add(new Role(2, "User"));
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phoneNumber) {
        this.phone = phoneNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Addon> getAddons() {
        return addons;
    }

    public void setAddons(Set<Addon> addons) {
        this.addons = addons;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public boolean isAdmin() {
        return roles.stream().anyMatch(r -> r.getName().equals("Admin"));
    }

    public boolean isBlocked() {

        return status == 0;
    }

    public List<AddonRating> getAddonRatingsList() {
        return addonRatingsList;
    }

    public void setAddonRatingsList(List<AddonRating> addonRatingsList) {
        this.addonRatingsList = addonRatingsList;
    }


    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {

        this.imageURI = imageURI;
    }

    public int getAddonsNumber() {
        return addonsNumber;
    }

    public void setAddonsNumber(int addonsNumber) {
        this.addonsNumber = addonsNumber;
    }

    public String getLastUploadDate() {
        return lastUploadDate;
    }

    public void setLastUploadDate(String lastUploadDate) {
        this.lastUploadDate = lastUploadDate;
    }
}
