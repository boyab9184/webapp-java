package com.alpha33.addonis.models;

import java.util.Date;

public class GitHubInfo {


    private String link;

    private int pullRequests;

    private int openIssues;

    private Date lastCommitDate;


    public GitHubInfo() {
    }


    public GitHubInfo(String link, int pullRequests, int openIssues, Date lastCommitDate) {
        this.link = link;
        this.pullRequests = pullRequests;
        this.openIssues = openIssues;
        this.lastCommitDate = lastCommitDate;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getPullRequests() {
        return pullRequests;
    }

    public void setPullRequests(int pullRequests) {
        this.pullRequests = pullRequests;
    }

    public int getOpenIssues() {
        return openIssues;
    }

    public void setOpenIssues(int openIssues) {
        this.openIssues = openIssues;
    }

    public Date getLastCommitDate() {
        return lastCommitDate;
    }

    public void setLastCommitDate(Date lastCommitDate) {
        this.lastCommitDate = lastCommitDate;
    }
}
