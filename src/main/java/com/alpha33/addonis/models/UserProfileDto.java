package com.alpha33.addonis.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserProfileDto {


    @NotBlank
    @Email(message = "Should enter email like someone@example.com")
    private String email;

    @NotBlank(message = "Phone number field should not be empty")
    @Size(min = 10, max = 10, message = "Phone number should be 10 digits")
    private String phone;

    public UserProfileDto() {
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
