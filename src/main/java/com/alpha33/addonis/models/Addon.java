package com.alpha33.addonis.models;

import com.alpha33.addonis.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.util.*;

@Entity
@Table(name = "addons")
public class Addon {

    public static final int INITIAL_DOWNLOAD_COUNT = 0;
    public static final double INITIAL_RATING_VALUE = 0;
    public static final int INITIAL_FEATURED_VALUE = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive(message = "Id must be positive")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ide_id")
    private Ide ide;

    @Column(name = "origin_location")
    private String originLocation;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "downloads")
    private Integer downloads;

    @Column(name = "description")
    private String description;

    @Column(name = "rating")
    private Double rating;

    @Transient
    private int roundedRate;

    @Transient
    private String imageURI;

    @Transient
    private int raters;

    public int getRoundedRate() {
        return roundedRate;
    }

    public void setRoundedRate(int roundedRate) {
        this.roundedRate = roundedRate;
    }

    @Column(name = "upload_date")
    private Date uploadDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    @Lob
    @Column(name = "image")
    private byte[] image;

    @JsonIgnore
    @Lob
    @Column(name = "binary_content")
    private byte[] content;

    @Column(name = "file_name")
    private String fileName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "addon", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<AddonRating> addonRatingsList;

    @Column(name = "featured")
    private Integer featured;

    public Addon() {
        this.status = Status.PENDING;
        this.downloads = INITIAL_DOWNLOAD_COUNT;
        this.rating = INITIAL_RATING_VALUE;
        this.uploadDate = Calendar.getInstance().getTime();
        this.featured = INITIAL_FEATURED_VALUE;
    }

    public Addon(Integer id, String name, Ide ide, String originLocation, Status status, Integer downloads,
                 String description, Double rating, Date uploadDate, User user, byte[] image, byte[] content,
                 String fileName, Integer featured) {
        this.id = id;
        this.name = name;
        this.ide = ide;
        this.originLocation = originLocation;
        this.status = Status.PENDING;
        this.downloads = INITIAL_DOWNLOAD_COUNT;
        this.description = description;
        this.rating = INITIAL_RATING_VALUE;
        this.uploadDate = Calendar.getInstance().getTime();
        this.user = user;
        this.image = image;
        this.content = content;
        this.fileName = fileName;
        this.featured = INITIAL_FEATURED_VALUE;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ide getIde() {
        return ide;
    }

    public void setIde(Ide ide) {
        this.ide = ide;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tagsList) {
        this.tags = tagsList;
    }

    public List<AddonRating> getAddonRatingsList() {
        return addonRatingsList;
    }

    public void setAddonRatingsList(List<AddonRating> addonRatingsList) {
        this.addonRatingsList = addonRatingsList;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public int getRaters() {
        return raters;
    }

    public void setRaters(int raters) {
        this.raters = raters;
    }

    public boolean isFeatured() {
        return featured > 0;
    }
}
