package com.alpha33.addonis;

import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.Ide;
import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.models.User;

import javax.persistence.Table;
import java.util.Calendar;
import java.util.Date;

public class TestHelpers {

    public static Addon createMockAddon() {
        var mockAddon = new Addon();
        mockAddon.setId(1);
        mockAddon.setIde(createMockIde());
        mockAddon.setName("mockAddonName");
        mockAddon.setFileName("mockAddonFileName");
        mockAddon.setDescription("mockAddonDescription");
        mockAddon.setOriginLocation("mockAddonOriginDescription");
        mockAddon.setDownloads(1);
        mockAddon.setFeatured(0);
        mockAddon.setRating(1.00);
        mockAddon.setUser(createMockUser());
        mockAddon.setImage(createMockByteImage());
        mockAddon.setContent(createMockByteContent());
        mockAddon.setUploadDate(getMockDate());
        return mockAddon;
    }

    public static byte[] createMockByteContent() {
        return new byte[10];
    }

    public static byte[] createMockByteImage() {
        return new byte[9];
    }

    public static Date getMockDate() {
        return Calendar.getInstance().getTime();
    }

    public static Ide createMockIde() {
        var mockIde = new Ide();
        mockIde.setId(1);
        mockIde.setName("mockIdeName");
        return mockIde;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setStatus(1);
        mockUser.setEmail("mockUserEmail");
        mockUser.setPhone("mockUserPhoneNumber");
        mockUser.setUsername("mockUsername");
        mockUser.setPassword("mockUserPassword");
        mockUser.setImageURI("photo");
        return mockUser;
    }

    public static User createMockUser2() {
        var mockUser2 = new User();
        mockUser2.setId(2);
        mockUser2.setStatus(1);
        mockUser2.setEmail("mockUserEmail2");
        mockUser2.setPhone("mockUserPhoneNumber2");
        mockUser2.setPassword("mockUserPassword2");
        return mockUser2;
    }

    public static Tag createMockTag() {
        var mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName("mockTagName");
        return mockTag;
    }


}
