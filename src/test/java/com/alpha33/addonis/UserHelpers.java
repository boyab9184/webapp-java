package com.alpha33.addonis;

import com.alpha33.addonis.models.Role;
import com.alpha33.addonis.models.User;

import java.util.HashSet;
import java.util.Set;

public class UserHelpers {

    public static User createMockAdmin() {
        var mockAdmin = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1, "Admin"));
        mockAdmin.setId(1);
        mockAdmin.setStatus(1);
        mockAdmin.setEmail("mockUserEmail");
        mockAdmin.setPhone("mockUserPhoneNumber");
        mockAdmin.setPassword("mockUserPassword");
        mockAdmin.setImageURI("photo");
        mockAdmin.setRoles(roles);

        return mockAdmin;
    }
}
