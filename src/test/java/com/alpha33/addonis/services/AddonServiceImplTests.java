package com.alpha33.addonis.services;

import com.alpha33.addonis.TestHelpers;
import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import com.alpha33.addonis.utils.ValidationHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AddonServiceImplTests {

    @Mock
    AddonRepository mockAddonRepository;

    @Mock
    ValidationHelper mockValidationHelper;

    @InjectMocks
    AddonServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockAddonRepository, Mockito.times(1)).getAll();
    }

    @Test
    void filter_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.filter(Optional.empty(),Optional.empty(),Optional.empty()))
                .thenReturn(new ArrayList<>());

        //Act
        service.filter(Optional.empty(),Optional.empty(),Optional.empty());

        //Assert
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .filter(Optional.empty(),Optional.empty(),Optional.empty());
    }

    @Test
    void getNew_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.getNew())
                .thenReturn(new ArrayList<>());

        //Act
        service.getNew();

        //Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .getNew();
    }

    @Test
    void getMostPopular_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.getMostPopular())
                .thenReturn(new ArrayList<>());

        //Act
        service.getMostPopular();

        //Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .getMostPopular();
    }

    @Test
    void getFeatured_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.getFeatured())
                .thenReturn(new ArrayList<>());

        //Act
        service.getFeatured();

        //Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .getFeatured();
    }

    @Test
    void getById_should_returnAddon_when_matchExists() {
        //Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        Mockito.when(mockAddonRepository.getById(mockAddon.getId()))
                .thenReturn(mockAddon);

        //Act
        Addon result = service.getById(mockAddon.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockAddon.getId(), result.getId()),
                () -> Assertions.assertEquals(mockAddon.getName(), result.getName()),
                () -> Assertions.assertEquals(mockAddon.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockAddon.getOriginLocation(), result.getOriginLocation()),
                () -> Assertions.assertEquals(mockAddon.getDownloads(), result.getDownloads()),
                () -> Assertions.assertEquals(mockAddon.getRating(), result.getRating()),
                () -> Assertions.assertEquals(mockAddon.getFeatured(), result.getFeatured()),
                () -> Assertions.assertEquals(mockAddon.getFileName(), result.getFileName()),
                () -> Assertions.assertEquals(mockAddon.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockAddon.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockAddon.getImage(), result.getImage()),
                () -> Assertions.assertEquals(mockAddon.getIde(), result.getIde()),
                () -> Assertions.assertEquals(mockAddon.getUploadDate(), result.getUploadDate())
        );
    }

    @Test
    void create_should_throw_when_addonWithSameFileNameExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        User mockUser = TestHelpers.createMockUser();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockAddon, mockUser));
    }

    @Test
    void create_should_throw_when_addonWithSameNameExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        User mockUser = TestHelpers.createMockUser();

        Mockito.when(mockAddonRepository.getByName(mockAddon.getName()))
                .thenReturn(mockAddon);

        // Act, Arrange
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockAddon, mockUser));
    }

    @Test
    void create_should_callRepository_when_addonWithSameNameAndSameFileNameDoesNotExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        User mockUser = TestHelpers.createMockUser();

        Mockito.when(mockAddonRepository.getByName(mockAddon.getName()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockAddonRepository.getByFileName(mockAddon.getFileName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockAddon, mockUser);

        // Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .create(mockAddon);
    }

    @Test
    void update_should_callRepository_when_userIsCreatorAndNotAdmin() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        User creator = mockAddon.getUser();


        Mockito.when(mockAddonRepository.getByName(mockAddon.getName()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockAddonRepository.getByFileName(mockAddon.getFileName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        mockValidationHelper.validateUserRights(creator);
        mockValidationHelper.validateUserAndCreator(mockAddon, creator);
        service.update(mockAddon, creator);

        // Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .update(mockAddon);
    }

    @Test
    void update_should_throw_when_addonWithSameNameAlreadyExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        mockAddon.setName("test-name");
        Addon anotherMockAddon = TestHelpers.createMockAddon();
        anotherMockAddon.setId(2);
        anotherMockAddon.setName("test-name");
        anotherMockAddon.setFileName("not-the-same-fileName");

        Mockito.when(mockAddonRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockAddon);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockAddon, TestHelpers.createMockUser()));
    }

    @Test
    void update_should_throw_when_addonWithSameFileNameExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        mockAddon.setFileName("test-fileName");
        Addon anotherMockAddon = TestHelpers.createMockAddon();
        anotherMockAddon.setId(2);
        anotherMockAddon.setName("not-the-same-name");
        anotherMockAddon.setFileName("test-fileName");

        Mockito.when(mockAddonRepository.getByName(Mockito.anyString()))
                        .thenReturn(mockAddon);
        Mockito.when(mockAddonRepository.getByFileName(Mockito.anyString()))
                .thenReturn(anotherMockAddon);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockAddon, TestHelpers.createMockUser()));
    }

    @Test
    void delete_should_callRepository_when_addonExists() {
        // Arrange
        Addon mockAddon = TestHelpers.createMockAddon();
        User creator = TestHelpers.createMockUser();

        mockValidationHelper.validateUserRights(creator);
        mockValidationHelper.validateUserAndCreator(mockAddon, creator);

        // Act
        service.delete(mockAddon.getId(), creator);

        // Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .delete(mockAddon.getId());
    }

    @Test
    void getAllApproved_should_callRepository() {
        //Arrange
        Mockito.when(mockAddonRepository.getAllApproved())
                .thenReturn(new ArrayList<>());

        //Act
        service.getAllApproved();

        //Arrange
        Mockito.verify(mockAddonRepository, Mockito.times(1))
                .getAllApproved();
    }
}
