package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.DuplicateEntityException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.repositories.contracts.UserRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.alpha33.addonis.TestHelpers.createMockUser;
import static com.alpha33.addonis.TestHelpers.createMockUser2;
import static com.alpha33.addonis.UserHelpers.createMockAdmin;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl mockService;

    @Test
    void getAll_should_callRepository() {
        // Arrange

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        mockService.getAll(createMockAdmin());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    public void getUserById_should_returnUser_when_matchExist() {

        // Arrange
        User mockCustomer = createMockUser();

        Mockito.when(mockRepository.getById(mockCustomer.getId()))
                .thenReturn(mockCustomer);
        // Act
        User result = mockService.getById(createMockAdmin(), mockCustomer.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCustomer.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()));

    }

    @Test
    public void getUserByEmail_should_returnUser_when_matchExist() {

        // Arrange
        User mockCustomer = createMockUser();

        Mockito.when(mockRepository.getByEmail(mockCustomer.getEmail()))
                .thenReturn(mockCustomer);
        // Act
        User result = mockService.getByEmail(mockCustomer.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCustomer.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()));

    }

    @Test
    public void getUserByUsername_should_returnUser_when_matchExist() {

        // Arrange
        User mockCustomer = createMockUser();

        Mockito.when(mockRepository.getByUsername(mockCustomer.getUsername()))
                .thenReturn(mockCustomer);
        // Act
        User result = mockService.getByUsername(mockCustomer.getUsername());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCustomer.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()));

    }

    @Test
    public void getUserByPhone_should_returnUser_when_matchExist() {

        // Arrange
        User mockCustomer = createMockUser();

        Mockito.when(mockRepository.getByPhone(mockCustomer.getPhone()))
                .thenReturn(mockCustomer);
        // Act
        User result = mockService.getByPhone(mockCustomer.getPhone());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCustomer.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()));
    }

    @Test
    public void create_should_throw_when_emailWithSameEmailExists() {
        // Arrange
        User mockCustomer = createMockUser();


        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> mockService.create(mockCustomer));
    }

    @Test
    public void create_should_callRepository_when_customerWithSameEmailUsernamePhoneDoesNotExist() {
        // Arrange
        User mockCustomer = createMockUser();


        Mockito.when(mockRepository.getByUsername(mockCustomer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByEmail(mockCustomer.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByPhone(mockCustomer.getPhone()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        mockService.create(mockCustomer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCustomer);
    }

    @Test
    public void update_should_callRepository_when_customerWithSameUsernameDoesNotExist() {
        // Arrange
        User mockCustomer = createMockUser();


        Mockito.when(mockRepository.getByEmail(mockCustomer.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByPhone(mockCustomer.getPhone()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        mockService.update(mockCustomer, mockCustomer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCustomer);
    }

//    @Test
//    public void update_should_Throw_when_customerWithSameUsernameExist() {
//        // Arrange
//        User mockUser = createMockUser();
//
//        User mockUser2 = createMockAdmin();
//
//
//        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
//                .thenThrow(EntityNotFoundException.class);
//
//        Mockito.when(mockRepository.getByPhone(mockUser2.getPhone()))
//                .thenThrow(EntityNotFoundException.class);
//
//        // Act
////        mockService.update(mockUser, mockUser2);
//
//        // Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> mockService.update(mockUser2, mockUser));
//    }

    @Test
    public void updatePassword_should_callRepository_when_passwordMatching() {
        // Arrange
        User mockCustomer = createMockUser();

        // Act
        mockService.updatePassword(mockCustomer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCustomer);
    }

    @Test
    public void updatePhoto_should_callRepository_when_pass_valid_file() {
        // Arrange
        User mockCustomer = createMockUser();
        byte[] photo = new byte[5];
        mockCustomer.setImage(photo);

        // Act
        mockService.updatePhoto(mockCustomer, photo);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCustomer);
    }


//    @Test
//    public void getImage_should_returnString_when_matchExist() {
//
//        // Arrange
//        User mockCustomer = TestHelpers.createMockUser();
//
//        Mockito.when(mockRepository.getByPhone(mockCustomer.getPhone()))
//                .thenReturn(mockCustomer);
//        // Act
//        String result = mockService.getImage(mockCustomer);
//
//        // Assert
//        Assertions.assertAll(
//                () -> Assertions.assertEquals(mockCustomer.getImage(), result.getBytes(StandardCharsets.UTF_8)));
//    }


}
