package com.alpha33.addonis.services;

import com.alpha33.addonis.TestHelpers;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.Ide;
import com.alpha33.addonis.repositories.contracts.IdeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class IdeServiceImplTests {

    @Mock
    IdeRepository mockIdeRepository;

    @InjectMocks
    IdeServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockIdeRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockIdeRepository, Mockito.times(1)).getAll();
    }

    @Test
    void create_should_callRepository_when_ideWithSameNameDoesNotExists() {
        // Arrange
        Ide mockIde = TestHelpers.createMockIde();

        Mockito.when(mockIdeRepository.getByName(mockIde.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockIde);

        // Assert
        Mockito.verify(mockIdeRepository, Mockito.times(1))
                .create(mockIde);
    }
}
