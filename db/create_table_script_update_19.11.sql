create table ides
(
    id   int auto_increment,
    name varchar(20) not null,
    constraint ides_id_uindex
        unique (id),
    constraint ides_name_uindex
        unique (name)
);

alter table ides
    add primary key (id);

create table tags
(
    id   int auto_increment,
    name varchar(30) not null,
    constraint tags_id_uindex
        unique (id),
    constraint tags_name_uindex
        unique (name)
);

alter table tags
    add primary key (id);

create table user_roles
(
    id   int auto_increment,
    name varchar(20) not null,
    constraint user_types_id_uindex
        unique (id),
    constraint user_types_name_uindex
        unique (name)
);

alter table user_roles
    add primary key (id);

create table users
(
    id           int auto_increment,
    username     varchar(20) not null,
    password     varchar(20) not null,
    email        varchar(50) not null,
    phone_number int(10)     not null,
    status       int         not null,
    image        longblob    not null,
    constraint users_email_uindex
        unique (email),
    constraint users_id_uindex
        unique (id),
    constraint users_password_uindex
        unique (password),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username)
);

alter table users
    add primary key (id);

create table addons
(
    id              int auto_increment,
    name            varchar(30)                                                not null,
    ide_id          int                                                        not null,
    origin_location varchar(300)                                               not null,
    status          enum ('PENDING', 'APPROVED', 'DECLINED') default 'PENDING' null,
    downloads       int                                      default 0         null,
    description     longtext                                                   not null,
    rating          int                                      default 0         not null,
    upload_date     date                                                       not null,
    user_id         int                                                        not null,
    image           longblob                                                   not null,
    binary_content  longblob                                                   not null,
    file_name       varchar(30)                                                not null,
    constraint addon_id_uindex
        unique (id),
    constraint addon_name_uindex
        unique (name),
    constraint addons_file_name_uindex
        unique (file_name),
    constraint addon_ides_id_fk
        foreign key (ide_id) references ides (id),
    constraint addons_users_id_fk
        foreign key (user_id) references users (id)
);

create index addon_origin_location_id_fk
    on addons (origin_location);

alter table addons
    add primary key (id);

create table addons_tags
(
    addon_id int not null,
    tag_id   int not null,
    constraint `addons-tags_addon_id_fk`
        foreign key (addon_id) references addons (id),
    constraint `addons-tags_tags_id_fk`
        foreign key (tag_id) references tags (id)
);

create table users_ratedAddons
(
    user_id  int not null,
    addon_id int null,
    value    int not null,
    constraint users_addons_addon_id_fk
        foreign key (addon_id) references addons (id),
    constraint users_addons_users_id_fk
        foreign key (user_id) references users (id)
);

create table users_roles
(
    user_id int not null,
    role_id int null,
    constraint users_roles_user_types_id_fk
        foreign key (role_id) references user_roles (id),
    constraint users_roles_users_id_fk
        foreign key (user_id) references users (id)
);


