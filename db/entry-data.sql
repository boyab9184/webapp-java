insert into ides (id, name) VALUES (1, 'IntelliJ');
insert into ides (id, name) VALUES (2, 'NetBean');
insert into ides (id, name) VALUES (3, 'Eclipse');
insert into ides (id, name) VALUES (4, 'DrJava');

insert into roles (id, name) VALUES (1, 'Admin');
insert into roles (id, name) VALUES (2, 'User');

insert into users (id, username, password, email, phone_number, status, image) VALUES
    (1, 'mecho.puch', 'pass$1Admin', 'mechi.puch@telerik.com', 1231231231, 1, null);


