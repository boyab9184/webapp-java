# Addonis

Addonis is an Addons Registry web application. Here you can find an extention/addon for your IDE. The application is similar to JetBrains Registry, Visual Studio Marketplace and Eclipse Marketplace.

A register user can:
- Publish their own addons
- Browse addons for their preferred IDE
- Download addons
- Rate existing addons

An admin can:
- Approve/decline addons
- Feature some addons
- Block unblock/user
- Change addon status



![Alt text](/src/main/resources/templates/assets/Screen Shot 2021-12-14 at 1.20.36 PM.png?raw=true "Index page")

_Instructions how to setup and run the project_:

**Option 1**

Click on Addonis it project link https://webapp.boyanbenev.com. Register as user and login or login as admin: username:admin, password:Super123!

**Option 2**
Use Docker.

```
docker run -d -p 80:8081 najsladkoto/addonis2020:v3
```
Then open your browser http://localhost


**Swagger**

You can find API docs on /swagger-ui.html


